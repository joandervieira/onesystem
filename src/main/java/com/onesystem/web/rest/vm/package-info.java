/**
 * View Models used by Spring MVC REST controllers.
 */
package com.onesystem.web.rest.vm;
